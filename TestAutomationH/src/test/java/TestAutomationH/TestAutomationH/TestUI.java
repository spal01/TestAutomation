package TestAutomationH.TestAutomationH;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TestUI {
  
	//========Configuration for Chrome Driver=======	
	String relativePath="driver/chromedriver.exe";
	String absolutePath="";
	ChromeDriver cdriver=null;
	File fl=null;
	String url="";
	
	//========Configuration for Extent Reports=======
	ExtentReports extReport;
	ExtentTest logger;
	
	
	WebElement root1=null;

		
	@BeforeTest
	public void setup(){
		
		//===Configuration for Chrome Driver============  
		absolutePath=new File(relativePath).getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", absolutePath);
		cdriver=new ChromeDriver();
	//==========Configuration for Extent Report======
		extReport=new ExtentReports("D:\\Users\\Hackathon\\workspace\\TestAutomationH\\test-output\\ExtentReport.html",true);
		extReport.addSystemInfo("HostName","Sumata");
		extReport.loadConfig(new File("D:\\Users\\Hackathon\\workspace\\TestAutomationH\\extent-config.xml"));
		//=======URI======================== 
		url="";
	}

	@BeforeMethod
	public void beforeMethod(){
		
	}
		
	@Test(priority=1,enabled=true)
	  public void testApplyLoan() {	
		logger=extReport.startTest("testApplyLoan");
		//===========open url========
		cdriver.get(url);
		WebElement web=cdriver.findElement(By.tagName("dffdf"));
		root1=expandRootElement(web);
		root1.findElement(By.linkText("APPLY LOAN")).click();
		//Assert.assertEquals(30,30);
		logger.log(LogStatus.PASS,"is passed");
	}


	@Test(priority=2,enabled=true)
	public void testViewLoan() {
		
		logger=extReport.startTest("testViewLoan");
		cdriver.get(url);
		WebElement web=cdriver.findElement(By.tagName("dffdf"));
		root1=expandRootElement(web);
		root1.findElement(By.linkText("VIEW LOAN")).click();	
		//logger.log(LogStatus.PASS,"is passed");
	}


	@Test(priority=3,enabled=true)
	public void testBrokerProfile() {
		logger=extReport.startTest("testBrokerProfile");	
		cdriver.get(url);
		WebElement web=cdriver.findElement(By.tagName("dffdf"));
		root1=expandRootElement(web);
		root1.findElement(By.linkText("BROKER PROFILE")).click();

	
	}

	
	
	@AfterMethod
	public void afterMethod(ITestResult result){
		
		if(result.getStatus()==ITestResult.SUCCESS){

			logger.log(LogStatus.PASS,"Test result" + result.getName());

			extReport.endTest(logger);

		}
		else if(result.getStatus()==ITestResult.FAILURE){
			logger.log(LogStatus.FAIL,"Test result" + result.getName());
			extReport.endTest(logger);
		}

		extReport.endTest(logger);
		cdriver.close();
	}

	@AfterTest
	public void endTest(){
		extReport.flush();
		extReport.close();
	}

//================Resolve Shadow Element========
	public WebElement expandRootElement(WebElement element){
		  WebElement ele = (WebElement)((JavascriptExecutor)cdriver).executeScript("return arguments[0].shadowRoot",element);
				  	return ele;
	}
	
	
}
