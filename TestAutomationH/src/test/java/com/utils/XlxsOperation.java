package com.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.chrome.ChromeDriver;

public class XlxsOperation {
	
	//========Configuration for xlsx=======	
	String relativePath="";
	String absolutePath="";
	File fl=null;
	FileInputStream fis=null;
	XSSFWorkbook obXSSFwork=null;
	XSSFSheet sheet=null;
	Row row=null;
	Cell cell=null;
	
	public XlxsOperation(String relativePath) throws IOException{
		this.relativePath=relativePath;
		absolutePath=new File(relativePath).getAbsolutePath();
		fis=new FileInputStream(absolutePath);
		obXSSFwork=new XSSFWorkbook(fis);
	
	}
	
	public String getData(String sheetName,int rowIndex,int cellIndex){
		sheet=obXSSFwork.getSheet(sheetName);
		row=sheet.getRow(rowIndex);
		cell=row.getCell(cellIndex);
		String data="";
		if(cell.getCellType()==Cell.CELL_TYPE_STRING){
			data=cell.getStringCellValue();	
		}
		if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC){
			data=String.valueOf(data);
			
		}
		return data;
		
	}
	
	

}
