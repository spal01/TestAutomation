package com.api.test;

import java.io.File;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TestAPI {
	String uri="http://10.117.189.240:8084";
	String resourceData="";
	String fullResource="";
	
	
	RequestSpecification requestSpecification=null; 
	Response response=null;
	int actualStatus;
	int expectedStatus=200;
	
	
	
	//========Configuration for Extent Reports=======
	ExtentReports extReport;
	ExtentTest logger;

	
	@BeforeTest
	public void beforeTest(){
		//==========Configuration for Extent Report======
		extReport=new ExtentReports("D:\\Users\\Hackathon\\workspace\\TestAutomationH\\test-output\\ExtentReportForRest.html",true);
		extReport.addSystemInfo("HostName","Sumata");
		extReport.loadConfig(new File("D:\\Users\\Hackathon\\workspace\\TestAutomationH\\extent-config.xml"));		
	}
	
	
	
  @Test(enabled=true)
  public void testGET(){
	  
	  RestAssured.baseURI=uri;
	  requestSpecification=RestAssured.given();
	  resourceData="mortgage/loan";
	  fullResource=uri+"/"+resourceData;
	  
	  
	  response=requestSpecification.request(Method.GET,fullResource);
	  actualStatus=response.getStatusCode();
	  
	  response.getBody().print();	  
	  logger=extReport.startTest("testGET");
	  Assert.assertEquals(expectedStatus, actualStatus);
	  logger.log(LogStatus.PASS,"is passed");
	   
  }
  
  

  
  @Test(enabled=true)
  public void testPost(){
	   
	  String rbody="{\"amount\":" +"1200" + ",\"fname\":"+"hcl"+",\"lname\":"+"user32"+ ",\"dob\":"+"2013-10-10"+ ",\"loanType\":"+"v"+ ",\"propertyAddress\":"+"sez hcl tower3"+",\"monthlyIncome\":"+"2000000" +",\"monthlyExpenses\":"+"2000000"+",\"loanTerm\":"+"15"+",\"repaymentFrequency\":"+"15"+"}";
	  
	  RestAssured.baseURI=uri;
	  
	  requestSpecification=RestAssured.given().contentType("application/json").with().body(rbody);
	  
	  
	  resourceData="mortgage/loan";
	  fullResource=uri+"/"+resourceData;
	 
	  response=requestSpecification.request(Method.POST, fullResource);
	  actualStatus=response.getStatusCode();
  
	  logger=extReport.startTest("testPost");
	  Assert.assertEquals(expectedStatus, actualStatus);
	  logger.log(LogStatus.PASS,"is passed");
	   
  }
  
  
  
  
 
  @AfterMethod
  public void afterMethod(ITestResult result){ 	
  	if(result.getStatus()==ITestResult.SUCCESS){
  		logger.log(LogStatus.PASS,"Test result" + result.getName());
  		extReport.endTest(logger);
  	}
  	else if(result.getStatus()==ITestResult.FAILURE){
  		logger.log(LogStatus.FAIL,"Test result" + result.getName());
  		extReport.endTest(logger);
  	}
  	extReport.endTest(logger);
  }

  @AfterTest
  public void endTest(){
  	extReport.flush();
  	extReport.close();
  }

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
